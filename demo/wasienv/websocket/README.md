This program hosts a webassembly program that needs websocket support.

An example of a webassembly program that uses this websocket support 
can be found in FPC's demos: 
```
packages/wasm-utils/demo/websocket
```

You will also need the websocket server program that is part of FPC
```
packages/fcl-web/examples/websocket/server
```
