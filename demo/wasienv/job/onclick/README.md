This is a demo to show that you can set an event handler for a DOM element
from within a Webassembly program that uses JOB.

You need the job_web.pas unit which is distributed in the Free Pascal
sources, see packages/wasm-job/examples/job_web.pas

Compile both the host program and webassembly program, and then you can see
the onclick event in action.


